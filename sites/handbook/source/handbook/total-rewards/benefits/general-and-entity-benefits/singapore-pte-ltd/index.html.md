---
layout: handbook-page-toc
title: "GitLab Singapore Pte Ltd"
description: "Discover GitLab's benefits for team members in Singapore"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Medical

GitLab offers Supplementary Medical Insurance through AVIVA Health Insurance which covers 100% of team member and eligible dependents (spouse & kids) contributions of premiums for the following plans:

For an outline of the plan, please review the [MyBenefits Plus Overview](https://drive.google.com/file/d/1NwdN927uEDujro6XAMJTpCrya6BMv16V/view?usp=sharing). 

**Please note that Dental benefits for this plan are cashless at panel Dental Clinics only. Team Members should show their eCard at the dental clinic to utlize the plan.**

Team Members an submit claims through the SingLife ClaimsConnect app. [See instructions](https://drive.google.com/file/d/1hMnxb1yurTpP9dNbpXvC2tyvfEnFny6F/view?usp=sharing) on how to use the app and submit claims.

Policy Documents
1. [MyBenefits Plus Group Basic Medical + Major Medical](https://drive.google.com/file/d/19zTv4QLOMN-eZnKw266IkCo76pEzGMHv/view?usp=sharing)
2. [MyBenefits Plus Group Outpatient (GP + SP)](https://drive.google.com/file/d/1F5DNdKShKGsd4TwAEgqIpyvcXULpWL3J/view?usp=sharing)
3. [MyBenefits Plus Group Denticare](https://drive.google.com/file/d/18B1IYPSV6_id821swmUyqA9RhoPV8qMF/view?usp=sharing)

AvivaOnline IT Helpdesk Contact Information:
- Hotline: `1800 827 9955  or 6827 9955`
- Fax: `(65) 6827 7700`
- Email: `avivaonline_ithelpdesk@aviva-asia.com`
- Address: `Aviva Asia Pte Ltd, 4 Shenton Way #01-01 SGX Centre 2, Singapore 068807`

### Enrollment 

Team Members will be enrolled into the medical plans by Global Upside upon their start date.  

Team Members who experience a qualified status change can update their information by sending an email to Global Upside at benefitsops@globalupside.com.

When adding a new dependent to the plan, please include the following information in your request to Global Upside:
* Dependent Name
* Dependent Date of Birth
* NRIC Number
* Gender
* Nationality 

## Pension

GitLab does not plan to offer a supplementary private pension benefit at this time as Singapore has their Pension system called as [Central Provident Fund](https://www.cpf.gov.sg/members)

## Life Insurance and Disability

GitLab offers company paid [Group Term Life](https://drive.google.com/file/d/1yffWuxwXD_GGzs2MCbA9xgAbgAb4JC3S/view?usp=sharing) and [Group Personal Accident](https://drive.google.com/file/d/1h2DrjLvlpC3nhwMCuUwvmyR3MXSDSnvG/view?usp=sharing) through AVIVA. This covers Life Insurance and Accidental Death & Accidental Dismemberment.

## GitLab Singapore Pte Ltd Parental Leave

### Statutory Leave Entitlements

#### Maternity Leave

Team members who have been continuously employed by GitLab for at least 3 months immediately before the birth of their child may be entitled to either 16 weeks of government-paid maternity leave in accordance with the Child Development Co-Savings Act or 12 weeks of maternity leave in accordance with the Employment Act. Eligibility can be determined by visiting the [Ministry of Manpower](https://www.mom.gov.sg/employment-practices/leave/maternity-leave/eligibility-and-entitlement) website.

**Government-paid maternity leave entitlement**
- For 1st and 2nd births, team members are entitled to 8 weeks paid by their employer and 8 weeks paid by the government.
- The first 8 weeks of leave must be taken in one continuous block. The following 8 weeks may be broken up into different periods; however, it must end no later than 12 months from the child's date of birth.
- For [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) team members, this leave runs concurrently with GitLab Parental Leave. Team members must initiate their leave by selecting the `Parental Leave` option in PTO by Roots at least 30 days before leave starts.

**Applying for government paid maternity leave**
- Team members must submit a [GPML Declaration](https://www.profamilyleave.gov.sg/Documents/PDF/GPML1.pdf) to [the Absence Management team](leaves@gitlab.com) as soon as possible. 
- GitLab will continue to pay the team member's salary during their leave and [submit a claim for reimbursement](https://www.profamilyleave.gov.sg/Pages/GPML.aspx#What%20should%20I%20do%20as%20an%20employer?).
- For 1st and 2nd births, GitLab will apply for reimbursement for the 9th - 16th weeks of leave. For 3rd and subsequent births, GitLab will apply for reimbursement for the full 16 weeks.

**Maternity Leave entitlements under the Employment Act**
- Eligible team members may be entitled to 12 weeks of maternity leave. This entitlement includes 8 weeks of leave paid by their employer and 4 weeks of unpaid leave.

**To initiate leave:**
- Team members must submit their leave request through PTO by Roots by choosing the `Parental Leave` option. 
- For [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) team members, this leave runs concurrently with GitLab Parental Leave. If eligible, team members would receive up to 16 weeks of paid maternity leave. 

#### Paternity Leave

Team members who have been continuously employed by GitLab for at least 3 months immediately before their child's due date may be entitled to 2 weeks of government-paid paternity leave. Please visit the [Ministry of Manpower website](https://www.mom.gov.sg/employment-practices/leave/paternity-leave) for more information on eligibility.
- For [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) team members, this leave runs concurrently with GitLab Parental Leave. If eligible, team members would receive up to 16 weeks of paid paternity leave. 

**Applying for Government-Paid Paternity Leave**
- Team members must submit their leave request through PTO by Roots by choosing the `Parental Leave` option.
- Team members must submit a [GPPL Declaration](https://www.profamilyleave.gov.sg/Documents/PDF/GPPL1.pdf) to [the Absence Management team](leaves@gitlab.com) as soon as possible. 
- GitLab will continue to pay the team member's salary during their leave and [submit a claim for reimbursement](https://www.profamilyleave.gov.sg/Pages/GPML.aspx#What%20should%20I%20do%20as%20an%20employer?).

#### **Annual Leave** 
- Team members in Singapore are entitled to Fourteen (14) days of annual leave per year which accrues on a pro rata basis throughout the year. This leave runs concurrently with GitLab PTO. Team members should initiate leave by entering their time off into PTO by Roots and selecting the `Vacation` option.
- Team members must coordinate with their team prior to taking time off to ensure business continuity. The Company may in its absolute discretion rescind its approval for any leave applied for where the exigencies of work so require.
- Unused annual leave may not be carried forward from any year to a succeeding year except with the prior approval of the Company.
- Unless the Company approves or requires otherwise, annual leave may not be used to offset any notice periods related to a team member's resignation or termination.

#### **Other Leave**
- Team members in Singapore are entitled to such outpatient sick leave and hospitalization leave as provided in the Employment Act.
- Your entitlement (if any) to maternity leave and benefits, adoption leave, childcare leave, extended childcare leave, unpaid infant care leave, shared parental leave and paternity leave shall be as provided in the Child Development Co-Savings Act and the Employment Act.
