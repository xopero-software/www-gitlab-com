---
layout: markdown_page
title: "Third Party Risk Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

In order to minimize the risk associated with third party applications and services, the Security Risk Team has designed this procedure to document the intake, assessment, and monitoring of Third Party Risk Management activities.

## Scope 

The Third Party Risk Management procedure is applicable to any third party application or service being provided to GitLab. This includes, but is not limited to, third parties providing free or paid applications or software, professional services organizations and contractors, marketing service providers and field marketing, alliances and partnerships, and mergers and acquisitions. 

## Roles and Responsibilities

| Role | Responsibility |
| ------ | ------ |
| **Security Risk team** | Maintain a mechanism to intake and respond to Third Party Risk Management Activities |
| | Provide complete and accurate responses within documented SLA |
| | Document and report any risks or trends identified during Third Party Risk Management Activities |
| | Maintain Metrics |
| **Business or System Owner** | Complete the **General Information** section of the TPRM issue or<br>Complete the intake questions in Coupa|
| | Work with the Security Risk team to facilitate any follow up with the third party|
| | Accept or Remediate any observations identified |

## Third Party Risk Management Activities Overview

Regardless of the type of Third Party being requested, it is important to document the risk they might pose to GitLab. We calculate this by assessing the type of data they will store, process, view or transmit. This allows us to determine an initial risk score and ensure that the high risk Third Parties are reviewed more thoroughly and more often while lower risk Third Parties can be approved quickly. In order to centralize this process, the Security Risk has developed the following two-part procedure. 

It is important to note that the Third Party Risk Management procedure is meant to function **alongside** of GitLab's procurement and contracting processes and is not a **replacement** for any other required reviews and approvals. 

## Part 1- Risk Assessment

The purpose of the Risk Assessment Phase is to conduct an initial vetting of all Third Parties to determine their inherent level of risk. A Risk Assessment is conducted with **every new procurement request**. This may seem inefficient given the amount of repeat requests for certain Third Parties, however, it is critical that this is done to validate the data classification, applicable integrations and previously assigned inherent risk tier. Oftentimes scope may change between engagements and/or with software add-ons which could change the data classification or integrations. However, due to automation built into the process, the risk assessment actually takes less than 5 minutes to complete once all information is provided.

### Initiating a Risk Assessment


1. GitLab Team Members will follow the [Purchase Request](https://about.gitlab.com/handbook/finance/procurement/#-purchase-request-quick-guide)process to submit a request for a new or renewing third party. 
1. The Security Risk team will collect key information on the [type of data](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) the third party will have access to, what [systems](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) the third party might have access to or integrate with, and contact information for the third party's security team.
   * **When submitting through Coupa:** The requestor will asked to complete the questions when creating a new requisition. 
1. The Security Risk team utilizes this information to determine the inherent risk based on the methodology below. 
1. The Security Risk team applies labels to the TPRM issue and documents the inherent risk and data classification for reporting and analytics.

### Determining the Inherent Risk

The Security Risk team uses the below methodology to determine the inherent risk of the Third Party. 

| Inherent Risk  | Definition |
| --- | -- | 
| High | Red data is in scope and/or<br>A GitLab System that stores Red data is in scope | 
| Moderate | Orange data is in scope and/or<br>A GitLab System that stores Orange data is in scope | 
| Low | Yellow, Green or No data is in scope and/or<br>A GitLab System that stores Yellow or Green data is in scope | 

## Part 2- Security Assessment

Third Parties with **Moderate or High inherent risk** will require a Security Assessment. The purpose of the Security Assessment Phase is to dive deeper into the Third Party’s security controls and ensure they can meet or exceed our [Third Party Minimum Security standards](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-risk/third-party-minimum-security-standards.html). This process is done entirely by the Security Risk team and unless otherwise noted, there is no action from the business owner during this phase. This review is required annually or if the inherent risk tier elevates to moderate or high prior to the annual review. The Security Risk team has a 10 day SLA to complete the Third Party Security Assessment based on the documentation received.

### Conducting a Security Assessment

If a Security Assessment is deemed as necessary, the Security Risk team conducts the following steps:

1. Creates Audit in ZenGRC in draft status.
1. Sends the **Document Request** email through ZenGRC to collect preliminary details. A sample of this email is noted below for reference. 
1. Reviews SOC1 Type 2, SOC2 Type 2 or similar, as well as any applicable bridge letters. If a third party does not have a SOC report (or similar), a questionnaire will be required based on the [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/security-risk/third-party-minimum-security-standards.html). If the third party is in scope for SOX, any CUECs will be mapped to the applicable GitLab control within ZenGRC. 
1. Reviews external testing such as independent vulnerability scan reports or penetration test reports.
1. Utilizes BitSight to obtain the security rating and documents any adverse findings. Third Parties with significant adverse findings and/or Third Parties with access to Red data will be escalated to the Security Operations and Research Team for a deeper technical assessment. 
1. Documents control effectiveness and ability to meet [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/security-risk/third-party-minimum-security-standards.html).
1. Follows [GitLab's Observation Management](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/observation-management/-/blob/master/ZenGRC%20Issue%20(Observation)%20Field%20Definitions.md) process to report risks. 
1. Documents the Residual Risk based on any observations or risks identified through the assessment. 
1. Updates the TPRM issue, Procurement Issue and/or Coupa Requisition with Residual Risk and any applicable observations.

## Adhoc Risk Assessments

The Adhoc Risk Assessment process should be used when: 
- An application or service is requested outside of the Procurement Process such as free applications or plugin's
- An application or service was onboarded prior to the TPRM program being in place and has not undergone a Risk Assessment previously
- An incident or concerns is raised related to a vendor, application or service

To request an Adhoc Risk Assessment, GitLab team members can open a [Third Party Risk Assessment issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-risk-team/third-party-vendor-security-management/-/issues/new?issuable_template=ThirdPartyRiskAssessment). The Security Risk Team will collect key information on the usage of the vendor or application, [type of data](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) and [systems](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) in scope, and the website for vendor. 

The Security Risk Team will follow the Risk Assessment process as described above. 

## Activity Location and Statuses
 
**GitLab**

1. Business Owner opens GitLab Procurement Issue.
1. Automation creates a TPRM Issue for each of the GitLab issues noted above. By default, the ~"Vendor Security Review" label is applied.
1. Business Owner completes the `General Information` Section of the TPRM issue.
1. Security Risk Calculates the Inherent Risk and applies the ~"VRA::Inherent_HIGH_risk", ~"VRA::Inherent_MODERATE_risk", or ~"VRA::Inherent_LOW_risk" labels, removes the ~"Vendor Security Review" label, and applies the ~"VSR Started" label. 
1. Security Risk ensures all areas in the `General Information` and `Inherent Risk Calculation` sections are completed.
1. Security Risk opens a ZenGRC Audit (pending future automation) and links it in the comments of the TPRM issue. Low inherent risk third parties are automatically closed within ZenGRC and GitLab as no further action is required. Moderate and high inherent risk third party audits are opened in `Draft` status and the GitLab issue will remain open.
1. After the Security Assessment is completed, Security Risk completes the `Residual Risk Score and Outstanding Observations` section of the TPRM issue.
1. Security Risk attaches a PDF copy of the Audit Report to the TPRM Issue and ensures the applicable Residual Risk label is applied. 
1. Security Risk updates the original Procurement issue to notify the Business Owner of any further action.

**Coupa**
/rebase
1. Business Owner opens Coupa request.
1. Business Owner answers the intake questions.
1. Security Risk Calculates the Inherent Risk.
1. Security Risk notifies the Business Owner of any next steps.
1. Security Risk opens a ZenGRC Audit (pending future automation) and links it in the comments of the Coupa request. Low inherent risk third parties are automatically closed within ZenGRC and approved in Coupa as no further action is required. Moderate and high inherent risk third party audits are opened in `Draft` status and the Coupa request will remain open. 
1. After the Security Assessment is completed, Security Risk comments back in the Coupa requisition with any Residual Risk and any Outstanding Observations
1. Security Risk attaches a PDF copy of the Audit Report to the Coupa Request and ensures the applicable Residual Risk tag is applied. 
1. Security Risk notifies the Business Owner of any further action 
 
**ZenGRC**

1. Security Risk sends `Documentation Request` questionnaire in ZenGRC to the security contact for the third party and applies the label ~"Documents Requested".  **NOTE**: The Audit will stay in `Draft` with no auditor assigned until the questionnaire is completed. 
1. Once the documents are received, Security Risk updates the label to ~"Documents Attached" and moves the audit to `In Progress`. 
1. Security Risk completes the audit and documents and Issues or Risk based on [GitLab's Observation Management](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/observation-management/-/blob/master/ZenGRC%20Issue%20(Observation)%20Field%20Definitions.md)
1. Security Risk documents the Residual Risk and Period Completed fields on the audit object 
1. Security Risk exports the Audit Report results to the [TPRM Template](https://docs.google.com/document/d/1aQ-UezLKwbOStVIL1c8VgnQVsDq7XLv6vzQoMjVPDcA/edit?usp=sharing) and attaches a PDF copy to the audit in ZenGRC
 
## Email Request Template

This is an example of the email template that is sent out via ZenGRC automatically. If the third party does not receive the automated email, this email template will be used by the Security Risk team to request the necessary information. 

>As part of GitLab's Third Party Risk Management program, our Security Risk team assesses Third Parties and their applications or services, as applicable, to ensure they meet our [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/security-risk/third-party-minimum-security-standards.html). Please complete the linked Document Request as soon as possible so we can begin our Security Assessment. If you do not have these resources, please email security-assurance@gitlab.com and we will send you a link to our Third Party Security Assessment questionnaire. We appreciate your partnership in helping GitLab complete its due diligence requirements.

## Coming soon

* Instructions for reporting a security incident with a Third Party

## Service Level Agreements

Once the TPRM Issue is created, the Business Owner will complete the **General Information** section within 5 Business Days. If the Business Owner does not respond within that timeframe, the TPRM issue will be labeled as ~RA-BLOCKED and will be removed from the daily queue. It is the Business Owner's responsibility to complete the **General Information** section of a blocked issue, remove the ~RA-BLOCKED label, and tag @gitlab-com/gl-security/security-assurance/security-risk-team when it is completed. 

Once the documentation is is received, the Security Risk will complete the Security Assessment within **10 business days**. If the Third Party does not respond within that timeframe, any open items will be documented as Observations and presented to the requestor. 

## Exceptions

Exceptions to this procedure will be tracked as per the [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process).

## References
* [Third Party Minimum Security Standards](/handbook/engineering/security/security-assurance/security-risk/third-party-minimum-security-standards.html)
